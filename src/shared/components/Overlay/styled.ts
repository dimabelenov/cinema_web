import { styled } from 'styled-components';

import { DRAWER_WIDTH, PADDING_MAIN_CONTENT } from '@/styles/shared';
import { ThemeT } from '@/styles/theme';

export const Overlay__Wrapper = styled.div<ThemeT>`
	background: ${({ theme }) => theme.light.other.overlay};
	position: fixed;
	left: ${DRAWER_WIDTH + PADDING_MAIN_CONTENT}px;
	top: 0;
	width: calc(100% - ${DRAWER_WIDTH + PADDING_MAIN_CONTENT}px);
	height: 100%;
	z-index: 2;
`;
