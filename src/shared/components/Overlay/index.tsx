import { Overlay__Wrapper } from './styled';

type PropsT = {
	close: () => void;
};
export const Overlay = ({ close }: PropsT) => <Overlay__Wrapper onClick={close} />;
