import { useLocation } from 'react-router-dom';

export const useNav = () => {
	const location = useLocation();
	const mainRoute = location.pathname.split('/').slice(0, 2).join('/');
	return {
		mainRoute,
	};
};
