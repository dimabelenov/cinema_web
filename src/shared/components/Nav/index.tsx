import { ROUTES } from '@/routes';

import logo from '@/assets/icons/logo.svg';

import { Toolbar } from '@mui/material';

import { useNav } from './hooks/useNav';
import {
	Nav__Item,
	Nav__Link,
	Nav__List,
	Nav__LogoImg,
	Nav__LogoTitle,
	Nav__Wrapper,
} from './styled';

export const Nav = () => {
	const { mainRoute } = useNav();

	return (
		<Nav__Wrapper>
			<Toolbar>
				<Nav__LogoImg src={logo} alt="nahkar" />
				<Nav__LogoTitle>Nahkar</Nav__LogoTitle>
			</Toolbar>
			<Nav__List>
				{ROUTES.filter((route) => route.isNav).map((route) => (
					<Nav__Item key={route.path}>
						<Nav__Link key={route.path} to={route.path} $isActive={route.path === mainRoute}>
							{route.icon && (
								<img src={route.path === mainRoute ? route.activeIcon : route.icon} alt="" />
							)}
							{route.label}
						</Nav__Link>
					</Nav__Item>
				))}
			</Nav__List>
		</Nav__Wrapper>
	);
};
