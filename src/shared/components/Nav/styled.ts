import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';

import { ThemeT } from '@/styles/theme';

const activeLinkStyles = css`
	background: var(--Light-States-Active-Menu, linear-gradient(270deg, #264cc8 0%, #31a3fa 100%));
	border-radius: 0px 50px 50px 0px;
	box-shadow: 0px 4px 8px -4px rgba(58, 53, 65, 0.42);
	color: var(--Light-Common-White, #fff);
`;

export const Nav__Wrapper = styled.nav``;
export const Nav__LogoImg = styled.img``;
export const Nav__LogoTitle = styled.div<ThemeT>`
	color: ${({ theme }) => theme.light.text.primary};
	margin: 0 0 0 12px;
	font-size: 20px;
	font-weight: 700;
	line-height: 24px;
`;
export const Nav__List = styled.ul<ThemeT>``;
export const Nav__Item = styled.li<ThemeT>`
	margin: 0 0 6px;
	&:last-child {
		margin: 0;
	}
`;
export const Nav__Link = styled(Link)<ThemeT & { $isActive: boolean }>`
	display: flex;
	align-items: center;
	gap: 10px;
	color: ${({ theme }) => theme.light.text.primary};
	font-size: 16px;
	font-weight: 400;
	line-height: 24px;
	text-decoration: none;
	padding: 8px 21px;
	${({ $isActive }) => $isActive && activeLinkStyles};

	&:hover {
		border-radius: 0px 50px 50px 0px;
		background: ${({ $isActive }) =>
			!$isActive && 'var(--Light-Action-Hover, rgba(58, 53, 65, 0.04))'};
	}
`;
