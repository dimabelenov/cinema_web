import { getYear } from 'date-fns';

import { Footer__Wrapper } from './styled';

export const Footer = () => {
	return (
		<Footer__Wrapper>
			<p>© Copyright, {getYear(Date.now())}</p>
		</Footer__Wrapper>
	);
};
