import styled, { css } from 'styled-components';

import { HEADER_HEIGHT } from '@/styles/shared';

const activeSearchStyles = css`
	/* padding: 20px 0; */
	border-radius: 0px 0px 10px 10px;
	background: var(--Light-Background-Paper, #fff);
	box-shadow: 0px 4px 8px -4px rgba(58, 53, 65, 0.42);
	backdrop-filter: blur(10px);
`;

export const Search__Wrapper = styled.div<{ $isActive: boolean }>`
	display: flex;
	align-items: center;
	background: ${({ $isActive }) => ($isActive ? '#fff' : 'transparent')};
	position: relative;
	z-index: 5;
	width: 100%;
	${({ $isActive }) => $isActive && activeSearchStyles};
	img {
		position: ${({ $isActive }) => ($isActive ? 'absolute' : 'relative')};
	}
	input {
		display: ${({ $isActive }) => ($isActive ? 'block' : 'none')};
	}
`;
export const Search__Icon = styled.img`
	cursor: pointer;
	left: 22px;
`;
export const Search__CloseIcon = styled.img`
	cursor: pointer;
	position: absolute;
	right: 22px;
`;
export const Search__TextField = styled.input`
	width: 100%;
	height: ${HEADER_HEIGHT}px;
	font-size: 30px;
	padding: 0 50px;
	border: none;
	outline: none;
	border-radius: 0px 0px 10px 10px;
`;
