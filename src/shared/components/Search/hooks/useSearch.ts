import { useState } from 'react';

export const useSearch = () => {
	const [isSearchOpen, setIsSearchOpen] = useState(false);
	const closeSearch = () => {
		setIsSearchOpen(false);
	};
	const openSearch = () => {
		setIsSearchOpen(true);
	};
	return {
		isSearchOpen,
		closeSearch,
		openSearch,
	};
};
