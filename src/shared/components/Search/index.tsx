import closeIcon from '@assets/icons/close-icon.svg';
import searchIcon from '@assets/icons/search-icon.svg';

import { Overlay } from '../Overlay';
import { useSearch } from './hooks/useSearch';
import { Search__CloseIcon, Search__Icon, Search__TextField, Search__Wrapper } from './styled';

export const Search = () => {
	const { isSearchOpen, closeSearch, openSearch } = useSearch();
	return (
		<>
			<Search__Wrapper $isActive={isSearchOpen}>
				<Search__Icon src={searchIcon} alt="search" onClick={openSearch} />
				<Search__TextField type="text" />
				{isSearchOpen && <Search__CloseIcon src={closeIcon} alt="close" onClick={closeSearch} />}
			</Search__Wrapper>
			{isSearchOpen && <Overlay close={closeSearch} />}
		</>
	);
};
