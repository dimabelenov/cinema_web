import { Providers } from '@/app/Providers';
import type { Meta, StoryObj } from '@storybook/react';

import { StatisticCard } from '.';

const meta = {
	title: 'Cards/StatisticCard',
	component: StatisticCard,
	parameters: {
		layout: 'padded',
		docs: {
			description: {
				component: 'Search component',
			},
		},
	},
	argTypes: {},
	tags: ['autodocs'],
	decorators: [
		(Story) => {
			return (
				<Providers>
					<Story />
				</Providers>
			);
		},
	],
} satisfies Meta<typeof StatisticCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
	args: {
		title: 'Movies',
		count: 100,
	},
};
