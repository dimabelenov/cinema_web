import { StatisticCard__Count, StatisticCard__Title, StatisticCard__Wrapper } from './styled';

type PropsT = {
	title: string;
	count: number;
};
export const StatisticCard = ({ title, count }: PropsT) => {
	return (
		<StatisticCard__Wrapper>
			<StatisticCard__Title>{title}</StatisticCard__Title>
			<StatisticCard__Count>{count}</StatisticCard__Count>
		</StatisticCard__Wrapper>
	);
};
