import styled from 'styled-components';

export const StatisticCard__Wrapper = styled.div`
	display: flex;
	min-width: 170px;
	max-width: 270px;
	width: max-content;
	justify-content: space-between;
	border-radius: 6px;
	background: var(--Light-Background-Paper, #fff);

	/* Light/Elevation/Card */
	box-shadow: 0px 2px 10px 0px rgba(58, 53, 65, 0.1);
	padding: 20px;
`;
export const StatisticCard__Title = styled.p``;
export const StatisticCard__Count = styled.p``;
