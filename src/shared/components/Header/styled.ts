import styled from 'styled-components';

import { AppBar, Toolbar } from '@mui/material';

export const Header__Wrapper = styled.header``;
export const Header__AppBar = styled(AppBar)`
	box-shadow: none !important;
`;
export const Header__Toolbar = styled(Toolbar)`
	background: ${({ theme }) => theme.light.other.bodyBackground};
`;
