import { Search } from '@components/Search';

import IconButton from '@mui/material/IconButton';

import { DRAWER_WIDTH } from '@/styles/shared';

import { Header__AppBar, Header__Toolbar, Header__Wrapper } from './styled';

type PropsT = {
	handleDrawerToggle: () => void;
};
export const Header = ({ handleDrawerToggle }: PropsT) => {
	return (
		<Header__Wrapper>
			<Header__AppBar
				position="fixed"
				sx={{
					width: { sm: `calc(100% - ${DRAWER_WIDTH}px)` },
					ml: { sm: `${DRAWER_WIDTH}px` },
				}}
			>
				<Header__Toolbar>
					<IconButton
						edge="start"
						onClick={handleDrawerToggle}
						sx={{ mr: 2, display: { sm: 'none' } }}
					>
						Menu
					</IconButton>
					<Search />
				</Header__Toolbar>
			</Header__AppBar>
		</Header__Wrapper>
	);
};
