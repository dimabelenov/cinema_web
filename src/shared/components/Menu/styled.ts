import styled from 'styled-components';

import { Drawer } from '@mui/material';

import { DRAWER_WIDTH } from '@/styles/shared';

export const Menu__Wrapper = styled.div`
	width: ${DRAWER_WIDTH}px;
	flex-shrink: 0;
`;

export const Menu__Drawer = styled(Drawer)`
	.MuiDrawer-paper {
		box-sizing: border-box;
		width: ${DRAWER_WIDTH}px;
		border: none;
		background: ${({ theme }) => theme.light.other.bodyBackground};
	}
`;
