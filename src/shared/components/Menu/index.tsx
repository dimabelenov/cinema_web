import { Nav } from '../Nav';
import { Menu__Drawer, Menu__Wrapper } from './styled';

type PropsT = {
	isMobileOpen: boolean;
	handleDrawerToggle: () => void;
};

export const Menu = ({ isMobileOpen, handleDrawerToggle }: PropsT) => {
	return (
		<Menu__Wrapper>
			<Menu__Drawer
				variant="temporary"
				open={isMobileOpen}
				onClose={handleDrawerToggle}
				ModalProps={{
					keepMounted: true,
				}}
				sx={{
					display: { xs: 'block', sm: 'none' },
				}}
			>
				<Nav />
			</Menu__Drawer>
			<Menu__Drawer
				variant="permanent"
				sx={{
					display: { xs: 'none', sm: 'block' },
				}}
				open
			>
				<Nav />
			</Menu__Drawer>
		</Menu__Wrapper>
	);
};
