import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import createUploadLink from 'apollo-upload-client/createUploadLink.mjs';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ThemeProvider } from 'styled-components';

import FontStyles from '@/styles/fontStyled';
import { GlobalStyle } from '@/styles/globalStyles';
import { theme } from '@/styles/theme';

export const Providers = ({ children }: { children: React.ReactNode }) => {
	const client = new ApolloClient({
		cache: new InMemoryCache(),
		link: createUploadLink({
			uri: import.meta.env.VITE_BASE_URL,
		}),
	});

	return (
		<ApolloProvider client={client}>
			<ToastContainer />
			<BrowserRouter>
				<ThemeProvider theme={theme}>
					<GlobalStyle theme={theme} />
					<FontStyles />
					{children}
				</ThemeProvider>
			</BrowserRouter>
		</ApolloProvider>
	);
};
