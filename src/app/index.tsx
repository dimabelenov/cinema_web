import { ROUTES } from '@/routes';
import { Navigate, Route, Routes } from 'react-router-dom';

import { AdminLayout } from '@/layouts/AdminLayout';

import { Providers } from './Providers';

export const App = () => (
	<Providers>
		<Routes>
			<Route path="/" element={<Navigate to="/dashboard" replace />} />
			<Route path="/" element={<AdminLayout />}>
				{ROUTES.map((route) => (
					<Route key={route.path} path={route.path} element={route.element} />
				))}
			</Route>
		</Routes>
	</Providers>
);
