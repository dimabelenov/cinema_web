import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';

export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = {
	[_ in K]?: never;
};
export type Incremental<T> =
	| T
	| { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
	ID: { input: string; output: string };
	String: { input: string; output: string };
	Boolean: { input: boolean; output: boolean };
	Int: { input: number; output: number };
	Float: { input: number; output: number };
	DateTime: { input: any; output: any };
	Upload: { input: any; output: any };
};

export type Actor = {
	__typename?: 'Actor';
	birth_date?: Maybe<Scalars['String']['output']>;
	created_at: Scalars['DateTime']['output'];
	first_name: Scalars['String']['output'];
	id: Scalars['String']['output'];
	img?: Maybe<Scalars['String']['output']>;
	last_name: Scalars['String']['output'];
	updated_at: Scalars['DateTime']['output'];
};

export type Category = {
	__typename?: 'Category';
	created_at: Scalars['DateTime']['output'];
	id: Scalars['String']['output'];
	movie?: Maybe<Array<Movie>>;
	title: Scalars['String']['output'];
	updated_at: Scalars['DateTime']['output'];
};

export type Movie = {
	__typename?: 'Movie';
	actors: Array<Actor>;
	categories: Array<Category>;
	created_at: Scalars['DateTime']['output'];
	description?: Maybe<Scalars['String']['output']>;
	id: Scalars['String']['output'];
	imdb?: Maybe<Scalars['Float']['output']>;
	img?: Maybe<Scalars['String']['output']>;
	title: Scalars['String']['output'];
	updated_at: Scalars['DateTime']['output'];
	year: Scalars['Float']['output'];
};

export type Mutation = {
	__typename?: 'Mutation';
	createActor: Actor;
	createCategory: Category;
	createMovie: Movie;
	deleteActor: Actor;
	deleteCategory: Category;
	deleteMovie: Movie;
	updateActor: Actor;
};

export type MutationCreateActorArgs = {
	birth_date?: InputMaybe<Scalars['String']['input']>;
	first_name: Scalars['String']['input'];
	img?: InputMaybe<Scalars['Upload']['input']>;
	last_name: Scalars['String']['input'];
};

export type MutationCreateCategoryArgs = {
	title: Scalars['String']['input'];
};

export type MutationCreateMovieArgs = {
	actors?: InputMaybe<Array<RelationInput>>;
	categories: Array<RelationInput>;
	description?: InputMaybe<Scalars['String']['input']>;
	imdb?: InputMaybe<Scalars['Float']['input']>;
	img?: InputMaybe<Scalars['Upload']['input']>;
	title: Scalars['String']['input'];
	year: Scalars['Int']['input'];
};

export type MutationDeleteActorArgs = {
	id: Scalars['String']['input'];
};

export type MutationDeleteCategoryArgs = {
	id: Scalars['String']['input'];
};

export type MutationDeleteMovieArgs = {
	id: Scalars['String']['input'];
};

export type MutationUpdateActorArgs = {
	birth_date?: InputMaybe<Scalars['String']['input']>;
	first_name?: InputMaybe<Scalars['String']['input']>;
	id: Scalars['String']['input'];
	img?: InputMaybe<Scalars['Upload']['input']>;
	last_name?: InputMaybe<Scalars['String']['input']>;
};

export type Query = {
	__typename?: 'Query';
	actor?: Maybe<Actor>;
	actors: Array<Actor>;
	categories: Array<Category>;
	movie: Movie;
	movies: Array<Movie>;
	statistic: Statistic;
};

export type QueryActorArgs = {
	id: Scalars['String']['input'];
};

export type QueryMovieArgs = {
	id: Scalars['String']['input'];
};

export type RelationInput = {
	id: Scalars['ID']['input'];
};

export type Statistic = {
	__typename?: 'Statistic';
	actors_count: Scalars['Float']['output'];
	categories_count: Scalars['Float']['output'];
	movies_count: Scalars['Float']['output'];
};

export type CreateActorMutationVariables = Exact<{
	first_name: Scalars['String']['input'];
	last_name: Scalars['String']['input'];
	birth_date: Scalars['String']['input'];
	img?: InputMaybe<Scalars['Upload']['input']>;
}>;

export type CreateActorMutation = {
	__typename?: 'Mutation';
	createActor: {
		__typename?: 'Actor';
		id: string;
		first_name: string;
		last_name: string;
		birth_date?: string | null;
		img?: string | null;
	};
};

export type CreateCategoryMutationVariables = Exact<{
	title: Scalars['String']['input'];
}>;

export type CreateCategoryMutation = {
	__typename?: 'Mutation';
	createCategory: { __typename?: 'Category'; id: string; title: string };
};

export type DeleteActorMutationVariables = Exact<{
	id: Scalars['String']['input'];
}>;

export type DeleteActorMutation = {
	__typename?: 'Mutation';
	deleteActor: {
		__typename?: 'Actor';
		id: string;
		first_name: string;
		last_name: string;
		birth_date?: string | null;
		img?: string | null;
	};
};

export type DeleteCategoryMutationVariables = Exact<{
	id: Scalars['String']['input'];
}>;

export type DeleteCategoryMutation = {
	__typename?: 'Mutation';
	deleteCategory: { __typename?: 'Category'; id: string; title: string };
};

export type DeleteMovieMutationVariables = Exact<{
	id: Scalars['String']['input'];
}>;

export type DeleteMovieMutation = {
	__typename?: 'Mutation';
	deleteMovie: {
		__typename?: 'Movie';
		id: string;
		title: string;
		year: number;
		imdb?: number | null;
		description?: string | null;
		img?: string | null;
	};
};

export type ActorQueryVariables = Exact<{
	id: Scalars['String']['input'];
}>;

export type ActorQuery = {
	__typename?: 'Query';
	actor?: {
		__typename?: 'Actor';
		id: string;
		first_name: string;
		last_name: string;
		birth_date?: string | null;
		img?: string | null;
		created_at: any;
		updated_at: any;
	} | null;
};

export type ActorsQueryVariables = Exact<{ [key: string]: never }>;

export type ActorsQuery = {
	__typename?: 'Query';
	actors: Array<{
		__typename?: 'Actor';
		id: string;
		first_name: string;
		last_name: string;
		birth_date?: string | null;
		img?: string | null;
		created_at: any;
		updated_at: any;
	}>;
};

export type CategoriesQueryVariables = Exact<{ [key: string]: never }>;

export type CategoriesQuery = {
	__typename?: 'Query';
	categories: Array<{
		__typename?: 'Category';
		id: string;
		title: string;
		created_at: any;
		updated_at: any;
	}>;
};

export type MovieQueryVariables = Exact<{
	id: Scalars['String']['input'];
}>;

export type MovieQuery = {
	__typename?: 'Query';
	movie: {
		__typename?: 'Movie';
		id: string;
		title: string;
		year: number;
		imdb?: number | null;
		description?: string | null;
		img?: string | null;
		created_at: any;
		updated_at: any;
		categories: Array<{
			__typename?: 'Category';
			id: string;
			title: string;
			created_at: any;
			updated_at: any;
		}>;
		actors: Array<{
			__typename?: 'Actor';
			id: string;
			first_name: string;
			last_name: string;
			birth_date?: string | null;
			img?: string | null;
			created_at: any;
			updated_at: any;
		}>;
	};
};

export type MoviesQueryVariables = Exact<{ [key: string]: never }>;

export type MoviesQuery = {
	__typename?: 'Query';
	movies: Array<{
		__typename?: 'Movie';
		id: string;
		title: string;
		year: number;
		imdb?: number | null;
		description?: string | null;
		img?: string | null;
		created_at: any;
		updated_at: any;
		categories: Array<{
			__typename?: 'Category';
			id: string;
			title: string;
			created_at: any;
			updated_at: any;
		}>;
	}>;
};

export type StatisticQueryVariables = Exact<{ [key: string]: never }>;

export type StatisticQuery = {
	__typename?: 'Query';
	statistic: {
		__typename?: 'Statistic';
		actors_count: number;
		movies_count: number;
		categories_count: number;
	};
};

export const CreateActorDocument = gql`
	mutation CreateActor(
		$first_name: String!
		$last_name: String!
		$birth_date: String!
		$img: Upload
	) {
		createActor(
			first_name: $first_name
			last_name: $last_name
			birth_date: $birth_date
			img: $img
		) {
			id
			first_name
			last_name
			birth_date
			img
		}
	}
`;
export type CreateActorMutationFn = Apollo.MutationFunction<
	CreateActorMutation,
	CreateActorMutationVariables
>;

/**
 * __useCreateActorMutation__
 *
 * To run a mutation, you first call `useCreateActorMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateActorMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createActorMutation, { data, loading, error }] = useCreateActorMutation({
 *   variables: {
 *      first_name: // value for 'first_name'
 *      last_name: // value for 'last_name'
 *      birth_date: // value for 'birth_date'
 *      img: // value for 'img'
 *   },
 * });
 */
export function useCreateActorMutation(
	baseOptions?: Apollo.MutationHookOptions<CreateActorMutation, CreateActorMutationVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useMutation<CreateActorMutation, CreateActorMutationVariables>(
		CreateActorDocument,
		options,
	);
}
export type CreateActorMutationHookResult = ReturnType<typeof useCreateActorMutation>;
export type CreateActorMutationResult = Apollo.MutationResult<CreateActorMutation>;
export type CreateActorMutationOptions = Apollo.BaseMutationOptions<
	CreateActorMutation,
	CreateActorMutationVariables
>;
export const CreateCategoryDocument = gql`
	mutation CreateCategory($title: String!) {
		createCategory(title: $title) {
			id
			title
		}
	}
`;
export type CreateCategoryMutationFn = Apollo.MutationFunction<
	CreateCategoryMutation,
	CreateCategoryMutationVariables
>;

/**
 * __useCreateCategoryMutation__
 *
 * To run a mutation, you first call `useCreateCategoryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCategoryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCategoryMutation, { data, loading, error }] = useCreateCategoryMutation({
 *   variables: {
 *      title: // value for 'title'
 *   },
 * });
 */
export function useCreateCategoryMutation(
	baseOptions?: Apollo.MutationHookOptions<CreateCategoryMutation, CreateCategoryMutationVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useMutation<CreateCategoryMutation, CreateCategoryMutationVariables>(
		CreateCategoryDocument,
		options,
	);
}
export type CreateCategoryMutationHookResult = ReturnType<typeof useCreateCategoryMutation>;
export type CreateCategoryMutationResult = Apollo.MutationResult<CreateCategoryMutation>;
export type CreateCategoryMutationOptions = Apollo.BaseMutationOptions<
	CreateCategoryMutation,
	CreateCategoryMutationVariables
>;
export const DeleteActorDocument = gql`
	mutation DeleteActor($id: String!) {
		deleteActor(id: $id) {
			id
			first_name
			last_name
			birth_date
			img
		}
	}
`;
export type DeleteActorMutationFn = Apollo.MutationFunction<
	DeleteActorMutation,
	DeleteActorMutationVariables
>;

/**
 * __useDeleteActorMutation__
 *
 * To run a mutation, you first call `useDeleteActorMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteActorMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteActorMutation, { data, loading, error }] = useDeleteActorMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteActorMutation(
	baseOptions?: Apollo.MutationHookOptions<DeleteActorMutation, DeleteActorMutationVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useMutation<DeleteActorMutation, DeleteActorMutationVariables>(
		DeleteActorDocument,
		options,
	);
}
export type DeleteActorMutationHookResult = ReturnType<typeof useDeleteActorMutation>;
export type DeleteActorMutationResult = Apollo.MutationResult<DeleteActorMutation>;
export type DeleteActorMutationOptions = Apollo.BaseMutationOptions<
	DeleteActorMutation,
	DeleteActorMutationVariables
>;
export const DeleteCategoryDocument = gql`
	mutation DeleteCategory($id: String!) {
		deleteCategory(id: $id) {
			id
			title
		}
	}
`;
export type DeleteCategoryMutationFn = Apollo.MutationFunction<
	DeleteCategoryMutation,
	DeleteCategoryMutationVariables
>;

/**
 * __useDeleteCategoryMutation__
 *
 * To run a mutation, you first call `useDeleteCategoryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCategoryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCategoryMutation, { data, loading, error }] = useDeleteCategoryMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteCategoryMutation(
	baseOptions?: Apollo.MutationHookOptions<DeleteCategoryMutation, DeleteCategoryMutationVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useMutation<DeleteCategoryMutation, DeleteCategoryMutationVariables>(
		DeleteCategoryDocument,
		options,
	);
}
export type DeleteCategoryMutationHookResult = ReturnType<typeof useDeleteCategoryMutation>;
export type DeleteCategoryMutationResult = Apollo.MutationResult<DeleteCategoryMutation>;
export type DeleteCategoryMutationOptions = Apollo.BaseMutationOptions<
	DeleteCategoryMutation,
	DeleteCategoryMutationVariables
>;
export const DeleteMovieDocument = gql`
	mutation DeleteMovie($id: String!) {
		deleteMovie(id: $id) {
			id
			title
			year
			imdb
			description
			img
		}
	}
`;
export type DeleteMovieMutationFn = Apollo.MutationFunction<
	DeleteMovieMutation,
	DeleteMovieMutationVariables
>;

/**
 * __useDeleteMovieMutation__
 *
 * To run a mutation, you first call `useDeleteMovieMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMovieMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMovieMutation, { data, loading, error }] = useDeleteMovieMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMovieMutation(
	baseOptions?: Apollo.MutationHookOptions<DeleteMovieMutation, DeleteMovieMutationVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useMutation<DeleteMovieMutation, DeleteMovieMutationVariables>(
		DeleteMovieDocument,
		options,
	);
}
export type DeleteMovieMutationHookResult = ReturnType<typeof useDeleteMovieMutation>;
export type DeleteMovieMutationResult = Apollo.MutationResult<DeleteMovieMutation>;
export type DeleteMovieMutationOptions = Apollo.BaseMutationOptions<
	DeleteMovieMutation,
	DeleteMovieMutationVariables
>;
export const ActorDocument = gql`
	query Actor($id: String!) {
		actor(id: $id) {
			id
			first_name
			last_name
			birth_date
			img
			created_at
			updated_at
		}
	}
`;

/**
 * __useActorQuery__
 *
 * To run a query within a React component, call `useActorQuery` and pass it any options that fit your needs.
 * When your component renders, `useActorQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useActorQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useActorQuery(
	baseOptions: Apollo.QueryHookOptions<ActorQuery, ActorQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useQuery<ActorQuery, ActorQueryVariables>(ActorDocument, options);
}
export function useActorLazyQuery(
	baseOptions?: Apollo.LazyQueryHookOptions<ActorQuery, ActorQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useLazyQuery<ActorQuery, ActorQueryVariables>(ActorDocument, options);
}
export function useActorSuspenseQuery(
	baseOptions?: Apollo.SuspenseQueryHookOptions<ActorQuery, ActorQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useSuspenseQuery<ActorQuery, ActorQueryVariables>(ActorDocument, options);
}
export type ActorQueryHookResult = ReturnType<typeof useActorQuery>;
export type ActorLazyQueryHookResult = ReturnType<typeof useActorLazyQuery>;
export type ActorSuspenseQueryHookResult = ReturnType<typeof useActorSuspenseQuery>;
export type ActorQueryResult = Apollo.QueryResult<ActorQuery, ActorQueryVariables>;
export const ActorsDocument = gql`
	query Actors {
		actors {
			id
			first_name
			last_name
			birth_date
			img
			created_at
			updated_at
		}
	}
`;

/**
 * __useActorsQuery__
 *
 * To run a query within a React component, call `useActorsQuery` and pass it any options that fit your needs.
 * When your component renders, `useActorsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useActorsQuery({
 *   variables: {
 *   },
 * });
 */
export function useActorsQuery(
	baseOptions?: Apollo.QueryHookOptions<ActorsQuery, ActorsQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useQuery<ActorsQuery, ActorsQueryVariables>(ActorsDocument, options);
}
export function useActorsLazyQuery(
	baseOptions?: Apollo.LazyQueryHookOptions<ActorsQuery, ActorsQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useLazyQuery<ActorsQuery, ActorsQueryVariables>(ActorsDocument, options);
}
export function useActorsSuspenseQuery(
	baseOptions?: Apollo.SuspenseQueryHookOptions<ActorsQuery, ActorsQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useSuspenseQuery<ActorsQuery, ActorsQueryVariables>(ActorsDocument, options);
}
export type ActorsQueryHookResult = ReturnType<typeof useActorsQuery>;
export type ActorsLazyQueryHookResult = ReturnType<typeof useActorsLazyQuery>;
export type ActorsSuspenseQueryHookResult = ReturnType<typeof useActorsSuspenseQuery>;
export type ActorsQueryResult = Apollo.QueryResult<ActorsQuery, ActorsQueryVariables>;
export const CategoriesDocument = gql`
	query Categories {
		categories {
			id
			title
			created_at
			updated_at
		}
	}
`;

/**
 * __useCategoriesQuery__
 *
 * To run a query within a React component, call `useCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useCategoriesQuery(
	baseOptions?: Apollo.QueryHookOptions<CategoriesQuery, CategoriesQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
}
export function useCategoriesLazyQuery(
	baseOptions?: Apollo.LazyQueryHookOptions<CategoriesQuery, CategoriesQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useLazyQuery<CategoriesQuery, CategoriesQueryVariables>(
		CategoriesDocument,
		options,
	);
}
export function useCategoriesSuspenseQuery(
	baseOptions?: Apollo.SuspenseQueryHookOptions<CategoriesQuery, CategoriesQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useSuspenseQuery<CategoriesQuery, CategoriesQueryVariables>(
		CategoriesDocument,
		options,
	);
}
export type CategoriesQueryHookResult = ReturnType<typeof useCategoriesQuery>;
export type CategoriesLazyQueryHookResult = ReturnType<typeof useCategoriesLazyQuery>;
export type CategoriesSuspenseQueryHookResult = ReturnType<typeof useCategoriesSuspenseQuery>;
export type CategoriesQueryResult = Apollo.QueryResult<CategoriesQuery, CategoriesQueryVariables>;
export const MovieDocument = gql`
	query Movie($id: String!) {
		movie(id: $id) {
			id
			title
			year
			imdb
			description
			img
			created_at
			updated_at
			categories {
				id
				title
				created_at
				updated_at
			}
			actors {
				id
				first_name
				last_name
				birth_date
				img
				created_at
				updated_at
			}
		}
	}
`;

/**
 * __useMovieQuery__
 *
 * To run a query within a React component, call `useMovieQuery` and pass it any options that fit your needs.
 * When your component renders, `useMovieQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMovieQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMovieQuery(
	baseOptions: Apollo.QueryHookOptions<MovieQuery, MovieQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useQuery<MovieQuery, MovieQueryVariables>(MovieDocument, options);
}
export function useMovieLazyQuery(
	baseOptions?: Apollo.LazyQueryHookOptions<MovieQuery, MovieQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useLazyQuery<MovieQuery, MovieQueryVariables>(MovieDocument, options);
}
export function useMovieSuspenseQuery(
	baseOptions?: Apollo.SuspenseQueryHookOptions<MovieQuery, MovieQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useSuspenseQuery<MovieQuery, MovieQueryVariables>(MovieDocument, options);
}
export type MovieQueryHookResult = ReturnType<typeof useMovieQuery>;
export type MovieLazyQueryHookResult = ReturnType<typeof useMovieLazyQuery>;
export type MovieSuspenseQueryHookResult = ReturnType<typeof useMovieSuspenseQuery>;
export type MovieQueryResult = Apollo.QueryResult<MovieQuery, MovieQueryVariables>;
export const MoviesDocument = gql`
	query Movies {
		movies {
			id
			title
			year
			imdb
			description
			img
			created_at
			updated_at
			categories {
				id
				title
				created_at
				updated_at
			}
		}
	}
`;

/**
 * __useMoviesQuery__
 *
 * To run a query within a React component, call `useMoviesQuery` and pass it any options that fit your needs.
 * When your component renders, `useMoviesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMoviesQuery({
 *   variables: {
 *   },
 * });
 */
export function useMoviesQuery(
	baseOptions?: Apollo.QueryHookOptions<MoviesQuery, MoviesQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useQuery<MoviesQuery, MoviesQueryVariables>(MoviesDocument, options);
}
export function useMoviesLazyQuery(
	baseOptions?: Apollo.LazyQueryHookOptions<MoviesQuery, MoviesQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useLazyQuery<MoviesQuery, MoviesQueryVariables>(MoviesDocument, options);
}
export function useMoviesSuspenseQuery(
	baseOptions?: Apollo.SuspenseQueryHookOptions<MoviesQuery, MoviesQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useSuspenseQuery<MoviesQuery, MoviesQueryVariables>(MoviesDocument, options);
}
export type MoviesQueryHookResult = ReturnType<typeof useMoviesQuery>;
export type MoviesLazyQueryHookResult = ReturnType<typeof useMoviesLazyQuery>;
export type MoviesSuspenseQueryHookResult = ReturnType<typeof useMoviesSuspenseQuery>;
export type MoviesQueryResult = Apollo.QueryResult<MoviesQuery, MoviesQueryVariables>;
export const StatisticDocument = gql`
	query Statistic {
		statistic {
			actors_count
			movies_count
			categories_count
		}
	}
`;

/**
 * __useStatisticQuery__
 *
 * To run a query within a React component, call `useStatisticQuery` and pass it any options that fit your needs.
 * When your component renders, `useStatisticQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useStatisticQuery({
 *   variables: {
 *   },
 * });
 */
export function useStatisticQuery(
	baseOptions?: Apollo.QueryHookOptions<StatisticQuery, StatisticQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useQuery<StatisticQuery, StatisticQueryVariables>(StatisticDocument, options);
}
export function useStatisticLazyQuery(
	baseOptions?: Apollo.LazyQueryHookOptions<StatisticQuery, StatisticQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useLazyQuery<StatisticQuery, StatisticQueryVariables>(StatisticDocument, options);
}
export function useStatisticSuspenseQuery(
	baseOptions?: Apollo.SuspenseQueryHookOptions<StatisticQuery, StatisticQueryVariables>,
) {
	const options = { ...defaultOptions, ...baseOptions };
	return Apollo.useSuspenseQuery<StatisticQuery, StatisticQueryVariables>(
		StatisticDocument,
		options,
	);
}
export type StatisticQueryHookResult = ReturnType<typeof useStatisticQuery>;
export type StatisticLazyQueryHookResult = ReturnType<typeof useStatisticLazyQuery>;
export type StatisticSuspenseQueryHookResult = ReturnType<typeof useStatisticSuspenseQuery>;
export type StatisticQueryResult = Apollo.QueryResult<StatisticQuery, StatisticQueryVariables>;
