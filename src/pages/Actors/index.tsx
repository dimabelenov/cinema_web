import { Loader } from '@/shared/components/Loader';

import { ActorTable } from '@/pages/Actors/components/ActorTable';

import { CreateActorModal } from './components/CreateActorModal';
import { useActors } from './hooks/useActors';

export const Actors = () => {
	const {
		actors,
		loading,
		openActorPage,
		editActor,
		deleteActor,
		isOpenCreateActorModal,
		openCreateActorModal,
		closeCreateActorModal,
		createActor,
	} = useActors();

	return (
		<div>
			{loading && <Loader />}
			<button onClick={openCreateActorModal}>+ Add Actor</button>
			{isOpenCreateActorModal && (
				<CreateActorModal
					onClose={closeCreateActorModal}
					isOpenModal={isOpenCreateActorModal}
					createActor={createActor}
				/>
			)}
			{actors && (
				<ActorTable
					actors={actors}
					openActorPage={openActorPage}
					editActor={editActor}
					deleteActor={deleteActor}
				/>
			)}
		</div>
	);
};
