export type CreateActorInputs = {
	first_name: string;
	last_name: string;
	birth_date: string;
	img: File[];
};
