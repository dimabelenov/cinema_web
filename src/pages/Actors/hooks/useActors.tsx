import { useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import {
	useActorsQuery,
	useCreateActorMutation,
	useDeleteActorMutation,
} from '@/generated/graphql';

import { CreateActorInputs } from '../types';

export const useActors = () => {
	const [isOpenCreateActorModal, setIsOpenCreateActorModal] = useState(false);
	const [createActorMutation, { loading: createActorLoading }] = useCreateActorMutation();
	const [deleteActorMutation, { loading: deleteActorLoading }] = useDeleteActorMutation();
	const createActor = useCallback(
		(args: CreateActorInputs) => {
			createActorMutation({
				variables: {
					first_name: args.first_name,
					last_name: args.last_name,
					birth_date: args.birth_date,
					img: args.img[0],
				},
				context: {
					headers: {
						'apollo-require-preflight': 'true',
					},
				},
				onCompleted: () => {
					setIsOpenCreateActorModal(false);
					toast.success('Actor created');
				},
				onError: () => {
					toast.error('Error creating actor');
				},
				refetchQueries: ['Actors'],
			});
		},
		[createActorMutation],
	);
	const deleteActor = useCallback(
		(actorId: string) => {
			deleteActorMutation({
				variables: {
					id: actorId,
				},
				onCompleted: () => {
					toast.success('Actor deleted');
				},
				onError: () => {
					toast.error('Error deleting actor');
				},
				refetchQueries: ['Actors'],
			});
		},
		[deleteActorMutation],
	);

	const openCreateActorModal = useCallback(() => {
		setIsOpenCreateActorModal(true);
	}, []);
	const closeCreateActorModal = useCallback(() => {
		setIsOpenCreateActorModal(false);
	}, []);
	const { data: dataActors, loading: actorsLoading } = useActorsQuery();
	const navigate = useNavigate();

	const openActorPage = useCallback(
		(id: string) => {
			navigate(`/actors/${id}`);
		},
		[navigate],
	);
	const editActor = useCallback((id: string) => {
		console.log(id);
	}, []);

	return {
		actors: dataActors?.actors,
		loading: actorsLoading || createActorLoading || deleteActorLoading,
		openActorPage,
		editActor,
		deleteActor,
		isOpenCreateActorModal,
		openCreateActorModal,
		closeCreateActorModal,
		createActor,
	};
};
