import { Dayjs } from 'dayjs';
import { useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';

import { Modal } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';

import { Modal__Row, Modal__Title, Modal__Wrapper } from '@/styles/shared';

import { CreateActorInputs } from '../../types';

type PropsT = {
	isOpenModal: boolean;
	onClose: () => void;
	createActor: (_data: CreateActorInputs) => void;
};
export const CreateActorModal = ({ isOpenModal, onClose, createActor }: PropsT) => {
	const [birthDate, setBirthDate] = useState<Dayjs | null>(null);
	const [isBirthDateError, setIsBirthDateError] = useState(false);
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<CreateActorInputs>();
	const onSubmit: SubmitHandler<CreateActorInputs> = (data) => {
		!isBirthDateError && createActor({ ...data, birth_date: getBirthDate() });
	};
	const getBirthDate = () => {
		return birthDate?.isValid() ? birthDate?.format('YYYY-MM-DD') : '';
	};

	const validHandler = () => {
		setIsBirthDateError(!birthDate?.isValid());
	};
	return (
		<Modal open={isOpenModal} onClose={onClose}>
			<Modal__Wrapper>
				<Modal__Row>
					<Modal__Title>Create Actor</Modal__Title>
				</Modal__Row>
				<form onSubmit={handleSubmit(onSubmit)}>
					<Modal__Row>
						<input {...register('first_name', { required: true })} />
						{errors.first_name && <div>This field is required</div>}
					</Modal__Row>
					<Modal__Row>
						<input {...register('last_name', { required: true })} />
						{errors.last_name && <div>This field is required</div>}
					</Modal__Row>
					<Modal__Row>
						<LocalizationProvider dateAdapter={AdapterDayjs}>
							<DatePicker
								label="Basic date picker"
								value={birthDate}
								onChange={(value) => {
									setIsBirthDateError(!value?.isValid());
									setBirthDate(value);
								}}
							/>
							{isBirthDateError && <div>This field is required</div>}
						</LocalizationProvider>
					</Modal__Row>
					<Modal__Row>
						<input {...register('img', { required: true })} type="file" />
						{errors.img && <div>This field is required</div>}
					</Modal__Row>
					<Modal__Row>
						{/* <input type="submit" /> */}
						<button onClick={validHandler}>Create</button>
					</Modal__Row>
				</form>
			</Modal__Wrapper>
		</Modal>
	);
};
