import {
	createColumnHelper,
	flexRender,
	getCoreRowModel,
	useReactTable,
} from '@tanstack/react-table';

import { Actor } from '@/generated/graphql';

type PropsT = {
	actors: Actor[];
	openActorPage: (_id: string) => void;
	editActor: (_id: string) => void;
	deleteActor: (_id: string) => void;
};
export const ActorTable = ({ actors, openActorPage, editActor, deleteActor }: PropsT) => {
	const columnHelper = createColumnHelper<Actor>();

	const columns = [
		columnHelper.accessor((row) => [row.first_name, row.last_name], {
			id: 'full_name',
			cell: (info) => {
				return (
					<span>
						{info.row.original.first_name} {info.row.original.last_name}
					</span>
				);
			},
			header: () => <span>Name</span>,
		}),
		columnHelper.accessor((row) => row.birth_date, {
			id: 'birth_date',
			cell: (info) => {
				return <span>{info.getValue()}</span>;
			},
			header: () => <span>Birth Date</span>,
		}),
		columnHelper.accessor((row) => row.id, {
			id: 'action',
			cell: (info) => {
				const id = info.getValue();
				return (
					<div>
						<button onClick={() => openActorPage(id)}>Open</button>
						<button onClick={() => editActor(id)}>Edit</button>
						<button onClick={() => deleteActor(id)}>Delete</button>
					</div>
				);
			},
			header: () => <span>Action</span>,
		}),
	];

	const table = useReactTable({
		data: actors,
		columns,
		getCoreRowModel: getCoreRowModel(),
	});

	return (
		<table>
			<thead>
				{table.getHeaderGroups().map((headerGroup) => (
					<tr key={headerGroup.id}>
						{headerGroup.headers.map((header) => (
							<th key={header.id}>
								{header.isPlaceholder
									? null
									: flexRender(header.column.columnDef.header, header.getContext())}
							</th>
						))}
					</tr>
				))}
			</thead>
			<tbody>
				{table.getRowModel().rows.map((row) => (
					<tr key={row.id}>
						{row.getVisibleCells().map((cell) => (
							<td key={cell.id}>{flexRender(cell.column.columnDef.cell, cell.getContext())}</td>
						))}
					</tr>
				))}
			</tbody>
		</table>
	);
};
