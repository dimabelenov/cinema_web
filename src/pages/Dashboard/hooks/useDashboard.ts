import { useStatisticQuery } from '@/generated/graphql';

export const useDashboard = () => {
	const { data: statistic, loading: statisticLoading } = useStatisticQuery();
	return {
		statistic: statistic?.statistic,
		loading: statisticLoading,
	};
};
