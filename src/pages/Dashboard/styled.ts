import { styled } from 'styled-components';

export const Dashboard__Wrapper = styled.div`
	/* padding: 16px; */
`;
export const Dashboard__StatisticList = styled.ul`
	display: flex;
	gap: 24px;
	flex-wrap: wrap;
`;
export const Dashboard__StatisticItem = styled.li``;
