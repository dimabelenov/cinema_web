import { Loader } from '@/shared/components/Loader';
import { StatisticCard } from '@/shared/components/StatisticCard';

import { useDashboard } from './hooks/useDashboard';
import { Dashboard__StatisticItem, Dashboard__StatisticList, Dashboard__Wrapper } from './styled';

export const Dashboard = () => {
	const { statistic, loading } = useDashboard();

	if (loading) {
		return <Loader />;
	}

	return (
		<Dashboard__Wrapper>
			{statistic && (
				<Dashboard__StatisticList>
					<Dashboard__StatisticItem>
						<StatisticCard title="Actors" count={statistic.actors_count} />
					</Dashboard__StatisticItem>
					<Dashboard__StatisticItem>
						<StatisticCard title="Movies" count={statistic.movies_count} />
					</Dashboard__StatisticItem>
					<Dashboard__StatisticItem>
						<StatisticCard title="Categories" count={statistic.categories_count} />
					</Dashboard__StatisticItem>
					<Dashboard__StatisticItem>
						<StatisticCard title="Categories" count={statistic.categories_count} />
					</Dashboard__StatisticItem>
				</Dashboard__StatisticList>
			)}
		</Dashboard__Wrapper>
	);
};
