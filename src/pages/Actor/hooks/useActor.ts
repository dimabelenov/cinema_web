import { useParams } from 'react-router-dom';

import { useActorQuery } from '@/generated/graphql';

export const useActor = () => {
	const { id } = useParams();
	const { data: actor, loading: actorLoading } = useActorQuery({
		variables: {
			id: id!,
		},
	});

	return {
		actor: actor?.actor,
		loading: actorLoading,
	};
};
