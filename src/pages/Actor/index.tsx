import { Loader } from '@/shared/components/Loader';

import { useActor } from './hooks/useActor';

export const Actor = () => {
	const { actor, loading } = useActor();
	return (
		<div>
			{loading && <Loader />}
			{actor && (
				<div>
					<h1>
						{actor.first_name} {actor.last_name}
					</h1>
					<h2>{actor.birth_date}</h2>
					{actor.img && <img src={actor.img} alt={`${actor.first_name} ${actor.last_name}`} />}
				</div>
			)}
		</div>
	);
};
