import { useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import {
	useCategoriesQuery,
	useCreateCategoryMutation,
	useDeleteCategoryMutation,
} from '@/generated/graphql';

import { CreateCategoryInputs } from '../types';

export const useCategories = () => {
	const [isOpenCreateCategoryModal, setIsOpenCreateCategoryModal] = useState(false);
	const { data: categoryData, loading: categoryLoading } = useCategoriesQuery();
	const [createCategoryMutation, { loading: createCategoryLoading }] = useCreateCategoryMutation();
	const [deleteCategoryMutation, { loading: deleteCategoryLoading }] = useDeleteCategoryMutation();
	const deleteCategory = useCallback(
		(actorId: string) => {
			deleteCategoryMutation({
				variables: {
					id: actorId,
				},
				onCompleted: () => {
					toast.success('Category deleted');
				},
				onError: () => {
					toast.error('Error deleting category');
				},
				refetchQueries: ['Categories'],
			});
		},
		[deleteCategoryMutation],
	);
	const createCategory = useCallback(
		(args: CreateCategoryInputs) => {
			createCategoryMutation({
				variables: {
					title: args.title,
				},
				context: {
					headers: {
						'apollo-require-preflight': 'true',
					},
				},
				onCompleted: () => {
					setIsOpenCreateCategoryModal(false);
					toast.success('Category created');
				},
				onError: () => {
					toast.error('Error creating category');
				},
				refetchQueries: ['Categories'],
			});
		},
		[createCategoryMutation],
	);
	const navigate = useNavigate();
	const openCategoryPage = useCallback(
		(id: string) => {
			navigate(`/categories/${id}`);
		},
		[navigate],
	);

	const openCreateCategoryModal = useCallback(() => {
		setIsOpenCreateCategoryModal(true);
	}, []);
	const closeCreateCategoryModal = useCallback(() => {
		setIsOpenCreateCategoryModal(false);
	}, []);
	return {
		categories: categoryData?.categories,
		loading: categoryLoading || createCategoryLoading || deleteCategoryLoading,
		openCategoryPage,
		openCreateCategoryModal,
		isOpenCreateCategoryModal,
		closeCreateCategoryModal,
		createCategory,
		deleteCategory,
	};
};
