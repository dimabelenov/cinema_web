import { Loader } from '@/shared/components/Loader';

import { CategoryTable } from './components/CategoryTable';
import { CreateCategoryModal } from './components/CreateCategoryModal';
import { useCategories } from './hooks/useCategories';

export const Categories = () => {
	const {
		loading,
		categories,
		openCategoryPage,
		openCreateCategoryModal,
		isOpenCreateCategoryModal,
		closeCreateCategoryModal,
		createCategory,
		deleteCategory,
	} = useCategories();

	return (
		<div>
			{loading && <Loader />}
			<button onClick={openCreateCategoryModal}>+ Add Category</button>
			{isOpenCreateCategoryModal && (
				<CreateCategoryModal
					onClose={closeCreateCategoryModal}
					isOpenModal={isOpenCreateCategoryModal}
					createCategory={createCategory}
				/>
			)}
			{categories && (
				<CategoryTable
					categories={categories}
					openCategoryPage={openCategoryPage}
					deleteCategory={deleteCategory}
				/>
			)}
		</div>
	);
};
