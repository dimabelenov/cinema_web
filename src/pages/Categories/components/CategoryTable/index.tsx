import {
	createColumnHelper,
	flexRender,
	getCoreRowModel,
	useReactTable,
} from '@tanstack/react-table';

import { Category } from '@/generated/graphql';

type PropsT = {
	categories: Category[];
	openCategoryPage: (_id: string) => void;
	deleteCategory: (_id: string) => void;
};
export const CategoryTable = ({ categories, openCategoryPage, deleteCategory }: PropsT) => {
	const columnHelper = createColumnHelper<Category>();

	const columns = [
		columnHelper.accessor((row) => [row.title], {
			id: 'title',
			cell: (info) => {
				return <span>{info.row.original.title}</span>;
			},
			header: () => <span>Title</span>,
		}),
		columnHelper.accessor((row) => row.id, {
			id: 'action',
			cell: (info) => {
				const id = info.getValue();
				return (
					<div>
						<button onClick={() => openCategoryPage(id)}>Open</button>
						<button>Edit</button>
						<button onClick={() => deleteCategory(id)}>Delete</button>
					</div>
				);
			},
			header: () => <span>Action</span>,
		}),
	];

	const table = useReactTable({
		data: categories,
		columns,
		getCoreRowModel: getCoreRowModel(),
	});

	return (
		<table>
			<thead>
				{table.getHeaderGroups().map((headerGroup) => (
					<tr key={headerGroup.id}>
						{headerGroup.headers.map((header) => (
							<th key={header.id}>
								{header.isPlaceholder
									? null
									: flexRender(header.column.columnDef.header, header.getContext())}
							</th>
						))}
					</tr>
				))}
			</thead>
			<tbody>
				{table.getRowModel().rows.map((row) => (
					<tr key={row.id}>
						{row.getVisibleCells().map((cell) => (
							<td key={cell.id}>{flexRender(cell.column.columnDef.cell, cell.getContext())}</td>
						))}
					</tr>
				))}
			</tbody>
		</table>
	);
};
