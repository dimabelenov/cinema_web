import { SubmitHandler, useForm } from 'react-hook-form';

import { Modal } from '@mui/material';

import { Modal__Row, Modal__Title, Modal__Wrapper } from '@/styles/shared';

import { CreateCategoryInputs } from '../../types';

type PropsT = {
	isOpenModal: boolean;
	onClose: () => void;
	createCategory: (_data: CreateCategoryInputs) => void;
};
export const CreateCategoryModal = ({ isOpenModal, onClose, createCategory }: PropsT) => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<CreateCategoryInputs>();
	const onSubmit: SubmitHandler<CreateCategoryInputs> = (data) => {
		createCategory(data);
	};

	return (
		<Modal open={isOpenModal} onClose={onClose}>
			<Modal__Wrapper>
				<Modal__Row>
					<Modal__Title>Create Category</Modal__Title>
				</Modal__Row>
				<form onSubmit={handleSubmit(onSubmit)}>
					<Modal__Row>
						<input {...register('title', { required: true })} />
						{errors.title && <div>This field is required</div>}
					</Modal__Row>
					<Modal__Row>
						<input type="submit" value="Create Category" />
					</Modal__Row>
				</form>
			</Modal__Wrapper>
		</Modal>
	);
};
