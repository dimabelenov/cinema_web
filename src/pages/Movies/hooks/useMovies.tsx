import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useDeleteMovieMutation, useMoviesQuery } from '@/generated/graphql';

export const useMovies = () => {
	const { data: dataMovies, loading: moviesLoading } = useMoviesQuery();
	const [deleteMovieMutation, { loading: deleteMovieLoading }] = useDeleteMovieMutation();
	const deleteMovie = useCallback(
		(actorId: string) => {
			deleteMovieMutation({
				variables: {
					id: actorId,
				},
				onCompleted: () => {
					toast.success('Movie deleted');
				},
				onError: () => {
					toast.error('Error deleting movie');
				},
				refetchQueries: ['Movies'],
			});
		},
		[deleteMovieMutation],
	);
	const navigate = useNavigate();
	const openMoviePage = useCallback(
		(id: string) => {
			navigate(`/movies/${id}`);
		},
		[navigate],
	);

	return {
		loading: deleteMovieLoading || moviesLoading,
		movies: dataMovies?.movies,
		openMoviePage,
		deleteMovie,
	};
};
