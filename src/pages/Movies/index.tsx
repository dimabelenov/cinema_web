import { Loader } from '@/shared/components/Loader';

import { MovieTable } from './components/MovieTable';
import { useMovies } from './hooks/useMovies';

export const Movies = () => {
	const { movies, loading, openMoviePage, deleteMovie } = useMovies();

	return (
		<div>
			{loading && <Loader />}
			<button>+ Add Movie</button>
			{movies && (
				<MovieTable movies={movies} openMoviePage={openMoviePage} deleteMovie={deleteMovie} />
			)}
		</div>
	);
};
