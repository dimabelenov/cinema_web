import {
	createColumnHelper,
	flexRender,
	getCoreRowModel,
	useReactTable,
} from '@tanstack/react-table';

import { MovieCategoryT } from '@/gql/types/movie';

type PropsT = {
	movies: MovieCategoryT[];
	openMoviePage: (_id: string) => void;
	deleteMovie: (_id: string) => void;
};

export const MovieTable = ({ movies, openMoviePage, deleteMovie }: PropsT) => {
	const columnHelper = createColumnHelper<MovieCategoryT>();
	const columns = [
		columnHelper.accessor((row) => row.title, {
			id: 'full_name',
			cell: (info) => {
				return <span>{info.row.original.title}</span>;
			},
			header: () => <span>Name</span>,
		}),
		columnHelper.accessor((row) => row.year, {
			id: 'year',
			cell: (info) => {
				return <span>{info.row.original.year}</span>;
			},
			header: () => <span>Year</span>,
		}),
		columnHelper.accessor((row) => row.imdb, {
			id: 'imdb',
			cell: (info) => {
				return <span>{info.row.original.imdb}</span>;
			},
			header: () => <span>IMDB</span>,
		}),
		columnHelper.accessor((row) => row.categories, {
			id: 'categories',
			cell: (info) => {
				return (
					<ul>
						{info.row.original.categories.map((category) => (
							<li key={category.id}>{category.title}</li>
						))}
					</ul>
				);
			},
			header: () => <span>Categories</span>,
		}),
		columnHelper.accessor((row) => row.id, {
			id: 'action',
			cell: (info) => {
				const id = info.getValue();
				return (
					<div>
						<button onClick={() => openMoviePage(id)}>Open</button>
						<button>Edit</button>
						<button onClick={() => deleteMovie(id)}>Delete</button>
					</div>
				);
			},
			header: () => <span>Action</span>,
		}),
	];

	const table = useReactTable({
		data: movies,
		columns,
		getCoreRowModel: getCoreRowModel(),
	});

	return (
		<table>
			<thead>
				{table.getHeaderGroups().map((headerGroup) => (
					<tr key={headerGroup.id}>
						{headerGroup.headers.map((header) => (
							<th key={header.id}>
								{header.isPlaceholder
									? null
									: flexRender(header.column.columnDef.header, header.getContext())}
							</th>
						))}
					</tr>
				))}
			</thead>
			<tbody>
				{table.getRowModel().rows.map((row) => (
					<tr key={row.id}>
						{row.getVisibleCells().map((cell) => (
							<td key={cell.id}>{flexRender(cell.column.columnDef.cell, cell.getContext())}</td>
						))}
					</tr>
				))}
			</tbody>
		</table>
	);
};
