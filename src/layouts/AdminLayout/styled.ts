import styled from 'styled-components';

import { DRAWER_WIDTH, HEADER_HEIGHT, PADDING_MAIN_CONTENT } from '@/styles/shared';
import { ThemeT } from '@/styles/theme';

export const AdminLayout__Wrapper = styled.div<ThemeT>`
	display: flex;
`;
export const AdminLayout__Content = styled.div<ThemeT>`
	flex-grow: 1;
	margin: ${HEADER_HEIGHT}px 0 0 0;
	padding: 0 ${PADDING_MAIN_CONTENT}px;
	width: calc(100% - ${DRAWER_WIDTH}px);
	background: ${({ theme }) => theme.light.other.bodyBackground};
`;
export const AdminLayout__ContentInner = styled.div<ThemeT>`
	/* border-radius: 6px;
	background: #fff;
	box-shadow: 0px 2px 10px 0px rgba(58, 53, 65, 0.1);
	min-height: calc(100vh - (${HEADER_HEIGHT}px + ${PADDING_MAIN_CONTENT}px)); */
`;
