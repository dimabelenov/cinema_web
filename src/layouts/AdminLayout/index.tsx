import { Header } from '@/shared/components/Header';
import { Menu } from '@/shared/components/Menu';
import { Outlet } from 'react-router-dom';

import { useAdminLayout } from './hooks/useAdminLayout';
import { AdminLayout__Content, AdminLayout__ContentInner, AdminLayout__Wrapper } from './styled';

export const AdminLayout = () => {
	const { isMobileOpen, handleDrawerToggle } = useAdminLayout();

	return (
		<AdminLayout__Wrapper>
			<Header handleDrawerToggle={handleDrawerToggle} />
			<Menu isMobileOpen={isMobileOpen} handleDrawerToggle={handleDrawerToggle} />
			<AdminLayout__Content>
				<AdminLayout__ContentInner>
					<Outlet />
				</AdminLayout__ContentInner>
			</AdminLayout__Content>
		</AdminLayout__Wrapper>
	);
};
