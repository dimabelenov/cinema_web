import { useState } from 'react';

export const useAdminLayout = () => {
	const [isMobileOpen, setIsMobileOpen] = useState(false);

	const handleDrawerToggle = () => {
		setIsMobileOpen(!isMobileOpen);
	};

	return {
		isMobileOpen,
		handleDrawerToggle,
	};
};
