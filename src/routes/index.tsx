import dashboardActiveIcon from '@assets/icons/dashboard-contrast-icon.svg';
import dashboardIcon from '@assets/icons/dashboard-icon.svg';

import { Actor } from '@/pages/Actor';
import { Actors } from '@/pages/Actors';
import { Categories } from '@/pages/Categories';
import { Category } from '@/pages/Category';
import { Dashboard } from '@/pages/Dashboard';
import { Movie } from '@/pages/Moovie';
import { Movies } from '@/pages/Movies';

export const ROUTES = [
	{
		path: '/dashboard',
		label: 'Dashboard',
		element: <Dashboard />,
		icon: dashboardIcon,
		activeIcon: dashboardActiveIcon,
		isNav: true,
	},
	{
		path: '/actors',
		label: 'Actors',
		element: <Actors />,
		icon: dashboardIcon,
		activeIcon: dashboardActiveIcon,
		isNav: true,
	},
	{
		path: '/movies',
		label: 'Movies',
		element: <Movies />,
		icon: dashboardIcon,
		activeIcon: dashboardActiveIcon,
		isNav: true,
	},
	{
		path: '/categories',
		label: 'Categories',
		element: <Categories />,
		icon: dashboardIcon,
		activeIcon: dashboardActiveIcon,
		isNav: true,
	},
	{
		path: '/actors/:id',
		label: 'Actor',
		element: <Actor />,
		icon: null,
		activeIcon: null,
		isNav: false,
	},
	{
		path: '/categories/:id',
		label: 'Actor',
		element: <Category />,
		icon: null,
		activeIcon: null,
		isNav: false,
	},
	{
		path: '/movies/:id',
		label: 'Movie',
		element: <Movie />,
		icon: null,
		activeIcon: null,
		isNav: false,
	},
];
