import { styled } from 'styled-components';

export const DRAWER_WIDTH = 260;
export const PADDING_MAIN_CONTENT = 24;
export const HEADER_HEIGHT = 64;

export const Modal__Wrapper = styled.div`
	background: #fff;
	width: 600px;
	position: absolute;
	left: 50%;
	top: 50%;
	transform: translate(-50%, -50%);
	padding: 16px;
	border-radius: 4px;
`;
export const Modal__Title = styled.h4`
	text-align: center;
`;
export const Modal__Row = styled.div`
	margin: 0 0 16px;
	input {
		width: 100%;
		height: 40px;
	}
`;
