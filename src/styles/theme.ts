export const theme = {
	light: {
		palate: {
			primary: {
				main: '#264CC8',
				light: '#4067E9',
				dark: '#1B368F',
				contrast: '#FFF',
				containedHoverBackground: '#264CC8',
				outlinedHoverBackground: 'rgba(231, 236, 252, 0.40)',
				outlinedRestingBackground: 'rgba(151, 172, 243, 0.50)',
			},
			secondary: {
				main: '#8A8D93',
				light: '#9C9FA4',
				dark: '#4D5056',
				contrast: '#FFF',
				containedHoverBackground: '#777B82',
				outlinedHoverBackground: 'rgba(138, 141, 147, 0.08)',
				outlinedRestingBackground: 'rgba(138, 141, 147, 0.50)',
			},
			info: {
				main: '#16B1FF',
				light: '#32BAFF',
				dark: '#0E71A3',
				contrast: '#FFF',
				containedHoverBackground: '#139CE0',
				outlinedHoverBackground: 'rgba(50, 186, 255, 0.08)',
				outlinedRestingBackground: 'rgba(50, 186, 255, 0.50)',
			},
			success: {
				main: '#56CA00',
				light: '#6AD01F',
				dark: '#378100',
				contrast: '#FFF',
				containedHoverBackground: '#4CB200',
				outlinedHoverBackground: 'rgba(86, 202, 0, 0.08)',
				outlinedRestingBackground: 'rgba(86, 202, 0, 0.50)',
			},
			warning: {
				main: '#FFB400',
				light: '#FFB547',
				dark: '#A37300',
				contrast: '#FFF',
				containedHoverBackground: '#E09E00',
				outlinedHoverBackground: 'rgba(255, 180, 0, 0.08)',
				outlinedRestingBackground: 'rgba(255, 180, 0, 0.50)',
			},
			error: {
				main: '#FF4C51',
				light: '#FF6166',
				dark: '#A33134',
				contrast: '#FFF',
				containedHoverBackground: '#E04347',
				outlinedHoverBackground: 'rgba(255, 76, 81, 0.08)',
				outlinedRestingBackground: 'rgba(255, 76, 81, 0.50)',
			},
		},
		text: {
			primary: 'rgba(80, 86, 95, 0.87)',
			secondary: 'rgba(58, 53, 65, 0.68)',
			disabled: 'rgba(58, 53, 65, 0.38)',
		},
		action: {
			active: 'rgba(58, 53, 65, 0.54)',
			hover: 'rgba(58, 53, 65, 0.04)',
			selected: 'rgba(58, 53, 65, 0.08)',
			disabled: 'rgba(58, 53, 65, 0.26)',
			disabledBg: 'rgba(58, 53, 65, 0.12)',
			focus: 'rgba(58, 53, 65, 0.12)',
		},
		other: {
			divider: 'rgba(58, 53, 65, 0.12)',
			outlineBorder: 'rgba(58, 53, 65, 0.23)',
			inputLine: 'rgba(58, 53, 65, 0.22)',
			overlay: 'rgba(58, 53, 65, 0.50)',
			snackbarBg: '#212121',
			bodyBackground: '#F4F5FA',
			paperCard: '#FFF',
		},
		customBg: {
			primary:
				'linear-gradient(0deg, rgba(255, 255, 255, 0.88) 0%, rgba(255, 255, 255, 0.88) 100%, #9155FD)',
			secondary:
				'linear-gradient(0deg, rgba(255, 255, 255, 0.88) 0%, rgba(255, 255, 255, 0.88) 100%, #8A8D93)',
			info: 'linear-gradient(0deg, rgba(255, 255, 255, 0.88) 0%, rgba(255, 255, 255, 0.88) 100%, #2196F3)',
			success:
				'linear-gradient(0deg, rgba(255, 255, 255, 0.88) 0%, rgba(255, 255, 255, 0.88) 100%, #4CAF50)',
			warning:
				'linear-gradient(0deg, rgba(255, 255, 255, 0.88) 0%, rgba(255, 255, 255, 0.88) 100%, #ED6C02)',
			error:
				'linear-gradient(0deg, rgba(255, 255, 255, 0.88) 0%, rgba(255, 255, 255, 0.88) 100%, #F44336)',
			menuActive: 'linear-gradient(270deg, #264CC8 0%, #31A3FA 100%)',
		},
		typography: {
			h1: {
				fontSize: 96,
				fontWeight: 112,
				lineHeight: 300,
			},
			h2: {
				fontSize: 60,
				fontWeight: 72,
				lineHeight: 300,
			},
		},
	},
	dark: {},
	breakpoints: {
		xs: 0,
		sm: 600,
		md: 900,
		lg: 1200,
		xl: 1536,
	},
};

export type ThemeT = {
	theme: typeof theme;
};
