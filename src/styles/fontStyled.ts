import InterBlack from '@assets/fonts/inter/Inter-Black.ttf';
import InterBold from '@assets/fonts/inter/Inter-Bold.ttf';
import InterExtraBold from '@assets/fonts/inter/Inter-ExtraBold.ttf';
import InterExtraLight from '@assets/fonts/inter/Inter-ExtraLight.ttf';
import Inter from '@assets/fonts/inter/Inter-ExtraLight.ttf';
import InterLight from '@assets/fonts/inter/Inter-Light.ttf';
import InterMedium from '@assets/fonts/inter/Inter-Medium.ttf';
import InterRegular from '@assets/fonts/inter/Inter-Regular.ttf';
import InterSemiBold from '@assets/fonts/inter/Inter-SemiBold.ttf';
import InterThin from '@assets/fonts/inter/Inter-Thin.ttf';
import { createGlobalStyle } from 'styled-components';

const FontStyles = createGlobalStyle`

  @font-face {
    font-family: 'Inter';
    src: url(${InterBlack}) format('truetype');
    font-weight: 900;
    font-style: normal;
  }

  @font-face {
    font-family: 'Inter';
    src: url(${InterExtraBold}) format('truetype');
    font-weight: 800;
    font-style: normal;
  }

  @font-face {
    font-family: 'Inter';
    src: url(${InterBold}) format('truetype');
    font-weight: 700;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'Inter';
    src: url(${InterSemiBold}) format('truetype');
    font-weight: 600;
    font-style: normal;
  }

  @font-face {
    font-family: 'Inter';
    src: url(${InterMedium}) format('truetype');
    font-weight: 500;
    font-style: normal;
  }

  @font-face {
    font-family: 'Inter';
    src: url(${InterRegular}) format('truetype');
    font-weight: 400;
    font-style: normal;
  }

  @font-face {
    font-family: 'Inter';
    src: url(${InterLight}) format('truetype');
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: 'Inter';
    src: url(${InterExtraLight}) format('truetype');
    font-weight: 200;
    font-style: normal;
  }

  @font-face {
    font-family: 'Inter';
    src: url(${Inter}) format('truetype');
    font-weight: 100;
    font-style: normal;
  }

  @font-face {
    font-family: 'Inter';
    src: url(${InterThin}) format('truetype');
    font-weight: 100;
    font-style: normal;
  }

`;

export default FontStyles;
