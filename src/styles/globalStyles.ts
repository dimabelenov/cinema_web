import { createGlobalStyle } from 'styled-components';

import { ThemeT } from './theme';

export const GlobalStyle = createGlobalStyle<ThemeT>`	
	* {
		box-sizing: border-box;
		font-family: 'Inter', sans-serif;
		margin: 0;
		padding: 0;
	}
	body {
		background: ${({ theme }) => theme.light.other.bodyBackground};
		@media (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
			// Apply for screens wider than or equal to sm
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.md}px) {
		// Apply for screens wider than or equal to md
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.lg}px) {
 		// Apply for screens wider than or equal to lg
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.xl}px) {
		// Apply for screens wider than or equal to xl
  }
	}
	ul {
		list-style: none;
	}
`;
