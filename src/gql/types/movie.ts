import { Movie } from '@/generated/graphql';

export type MovieCategoryT = Omit<Movie, 'actors'>;
