import { gql } from '@apollo/client';

export const GET_ACTORS = gql`
	query Actors {
		actors {
			id
			first_name
			last_name
			birth_date
			img
			created_at
			updated_at
		}
	}
`;
