import { gql } from '@apollo/client';

export const GET_MOVIES = gql`
	query Movies {
		movies {
			id
			title
			year
			imdb
			description
			img
			created_at
			updated_at
			categories {
				id
				title
				created_at
				updated_at
			}
		}
	}
`;
