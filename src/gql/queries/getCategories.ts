import { gql } from '@apollo/client';

export const GET_CATEGORIES = gql`
	query Categories {
		categories {
			id
			title
			created_at
			updated_at
		}
	}
`;
