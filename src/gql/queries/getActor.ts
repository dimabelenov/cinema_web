import { gql } from '@apollo/client';

export const GET_ACTOR = gql`
	query Actor($id: String!) {
		actor(id: $id) {
			id
			first_name
			last_name
			birth_date
			img
			created_at
			updated_at
		}
	}
`;
