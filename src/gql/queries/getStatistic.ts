import { gql } from '@apollo/client';

export const GET_STATISTIC = gql`
	query Statistic {
		statistic {
			actors_count
			movies_count
			categories_count
		}
	}
`;
