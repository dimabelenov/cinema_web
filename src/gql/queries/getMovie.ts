import { gql } from '@apollo/client';

export const GET_MOVIE = gql`
	query Movie($id: String!) {
		movie(id: $id) {
			id
			title
			year
			imdb
			description
			img
			created_at
			updated_at
			categories {
				id
				title
				created_at
				updated_at
			}
			actors {
				id
				first_name
				last_name
				birth_date
				img
				created_at
				updated_at
			}
		}
	}
`;
