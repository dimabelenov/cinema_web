import { gql } from '@apollo/client';

export const DELETE_Category = gql`
	mutation DeleteCategory($id: String!) {
		deleteCategory(id: $id) {
			id
			title
		}
	}
`;
