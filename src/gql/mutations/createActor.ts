import { gql } from '@apollo/client';

export const CREATE_ACTOR = gql`
	mutation CreateActor(
		$first_name: String!
		$last_name: String!
		$birth_date: String!
		$img: Upload
	) {
		createActor(
			first_name: $first_name
			last_name: $last_name
			birth_date: $birth_date
			img: $img
		) {
			id
			first_name
			last_name
			birth_date
			img
		}
	}
`;
