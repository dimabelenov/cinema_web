import { gql } from '@apollo/client';

export const DELETE_ACTOR = gql`
	mutation DeleteActor($id: String!) {
		deleteActor(id: $id) {
			id
			first_name
			last_name
			birth_date
			img
		}
	}
`;
