import { gql } from '@apollo/client';

export const CREATE_CATEGORY = gql`
	mutation CreateCategory($title: String!) {
		createCategory(title: $title) {
			id
			title
		}
	}
`;
