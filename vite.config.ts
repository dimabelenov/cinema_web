import react from '@vitejs/plugin-react';
import path from 'path';
import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
	resolve: {
		alias: {
			'@': path.resolve(__dirname, './src'),
			'@assets': path.resolve(__dirname, './src/assets'),
			'@config': path.resolve(__dirname, './src/config'),
			'@gql': path.resolve(__dirname, './src/gql'),
			'@generated': path.resolve(__dirname, './src/generated'),
			'@hooks': path.resolve(__dirname, './src/shared/hooks'),
			'@services': path.resolve(__dirname, './src/services'),
			'@utils': path.resolve(__dirname, './src/utils'),
			'@components': path.resolve(__dirname, './src/shared/components'),
			'@styles': path.resolve(__dirname, './src/styles'),
			'@layouts': path.resolve(__dirname, './src/layouts'),
			'@pages': path.resolve(__dirname, './src/pages'),
		},
	},
	plugins: [react()],
	server: {
		watch: {
			usePolling: true,
		},
		host: true,
		port: 5178,
		strictPort: true,
	},
});
